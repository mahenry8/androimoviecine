package com.example.moviecine.rest;

import com.example.moviecine.model.Cine;
import com.example.moviecine.model.Showtimes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MovieAPI {

    @GET("cine.json")
    Call<Cine> getShowTime();

    /**
     * Call a webservice with a static path
     */
    @GET("https://custom.server.com/file.json")
    Call<String> getFile();
}
