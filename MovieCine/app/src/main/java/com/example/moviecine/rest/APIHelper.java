package com.example.moviecine.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class to call API to get data
 */
public class APIHelper {
    private MovieAPI movieApi;

    /**
     * Get movies
     * @return
     */
    public MovieAPI getMoviesApi() {
        return movieApi;
    }

    /**
     * Call API
     */
    private APIHelper() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://etudiants.openium.fr/pam/").addConverterFactory(GsonConverterFactory.create()).build();
        movieApi = retrofit.create(MovieAPI.class);
    }

    private static volatile APIHelper instance;

    /**
     * Get instance of API helper
     * @return
     */
    public static synchronized APIHelper getInstance() {
        if (instance == null) {
            instance = new APIHelper();
        }
        return instance;
    }
}

