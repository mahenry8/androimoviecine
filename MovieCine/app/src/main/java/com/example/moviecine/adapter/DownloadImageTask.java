package com.example.moviecine.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import java.io.InputStream;

/**
 * Class used to load movie poster picture
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView imageView;

    /**
     * Public constructor
     * @param imageView
     */
    public DownloadImageTask(ImageView imageView) {
        this.imageView = imageView;
    }

    /**
     * Download in background the pictures
     * @param urls
     * @return
     */
    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    /**
     * Set result picture
     * @param result
     */
    protected void onPostExecute(Bitmap result) {
        imageView.setImageBitmap(result);
    }
}
