package com.example.moviecine.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.moviecine.R;
import com.example.moviecine.model.Showtimes;
import java.util.List;
import java.util.concurrent.TimeUnit;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Class for the movie Adapter
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {
    private List<Showtimes> movieShowtimes;
    private OnMovieClickedListener onMovieClickedListener;

    /**
     * Public constructor
     * @param movieShowtimes
     * @param onMovieClickedListener
     */
    public MovieAdapter(List<Showtimes> movieShowtimes, OnMovieClickedListener onMovieClickedListener) {
        this.movieShowtimes = movieShowtimes;
        this.onMovieClickedListener = onMovieClickedListener;
    }

    /**
     * Create the view
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MovieViewHolder(view);
    }

    /**
     * Bind movie with the one selected in the main activity
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        final Showtimes st = movieShowtimes.get(position);
        holder.name.setText(st.onShow.movie.title);
        holder.mark.setText(Float.toString(Math.round(st.onShow.movie.statistics.userRating * 100.0f)/ 100.0f)+"/5");
        holder.genre.setText(st.onShow.movie.genre.get(0).name);
        new DownloadImageTask(holder.picture).execute(st.onShow.movie.poster.href);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieAdapter.this.onMovieClickedListener.onMovieClicked(v, st);
            }
        });
    }

    /**
     * Count number of items
     * @return
     */
    @Override
    public int getItemCount() {
        return movieShowtimes.size();
    }

    /**
     * Set the fields in the view
     */
    static class MovieViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView mark;
        public TextView genre;
        public ImageView picture;

        public MovieViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            mark = view.findViewById(R.id.mark);
            genre = view.findViewById(R.id.genre);
            picture = view.findViewById(R.id.picture);
        }
    }

    /**
     * Handle when movie is selected
     */
    public interface OnMovieClickedListener {
        void onMovieClicked(View view, Showtimes movie);
    }
}


