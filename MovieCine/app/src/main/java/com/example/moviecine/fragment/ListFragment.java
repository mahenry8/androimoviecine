package com.example.moviecine.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.moviecine.R;
import com.example.moviecine.adapter.MovieAdapter;
import com.example.moviecine.model.Cine;
import com.example.moviecine.model.Showtimes;
import com.example.moviecine.rest.APIHelper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragment for the movies list view
 */
public class ListFragment extends Fragment implements MovieAdapter.OnMovieClickedListener {
    private RecyclerView recyclerViewMovie;

    /**
     * Create the view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_movies, container, false);
        recyclerViewMovie = view.findViewById(R.id.recycler_movie);
        return view;
    }

    /**
     * Handle when an activity is selected
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerViewMovie.setLayoutManager(new LinearLayoutManager(getContext()));
        displayMovieSync();
    }

    /**
     * Display movie fields
     */
    private void displayMovieSync() {
        APIHelper.getInstance().getMoviesApi().getShowTime().enqueue(new Callback<Cine>() {
            @Override
            public void onResponse(Call<Cine> call, Response<Cine> response) {
                if(response.isSuccessful()) {
                    recyclerViewMovie.setVisibility(View.VISIBLE);
                    recyclerViewMovie.setAdapter(new MovieAdapter(response.body().movieShowtimes, ListFragment.this));
                }
                else {
                    Log.e( "Toto", "Erreur http" );
                }
            }
            @Override
            public void onFailure(Call<Cine> call, Throwable t) {
                Log.e( "Toto", "Erreur reseau :"  + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    /**
     * Handle cwhen a movie is selected
     * @param view
     * @param movie
     */
    @Override
    public void onMovieClicked(View view, Showtimes movie) {
        DetailFragment fragment = DetailFragment.newInstance(movie);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, "DETAIL").commitAllowingStateLoss();
    }

}
