package com.example.moviecine.model;

/**
 * Showtimes Model
 */
public class Showtimes {

    public class OnShow {
        public Movie movie;
    }

    public String display;
    public OnShow onShow;

}
