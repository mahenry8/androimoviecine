package com.example.moviecine.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.moviecine.R;
import com.example.moviecine.adapter.DownloadImageTask;
import com.example.moviecine.model.Showtimes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * The fragment for the movie view
 */
public class DetailFragment extends Fragment {
    private Showtimes movie;

    /**
     * Create a new instance of the fragment
     * @param movie
     * @return
     */
    public static DetailFragment newInstance(Showtimes movie) {
        DetailFragment detailFragment = new DetailFragment();
        detailFragment.movie = movie;
        return  detailFragment;
    }

    /**
     * Create the view and set fields
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_movie, container, false);
        TextView title = view.findViewById(R.id.title);
        TextView author = view.findViewById(R.id.author);
        TextView actors = view.findViewById(R.id.actors);
        TextView releaseDate = view.findViewById(R.id.releaseDate);
        ImageView poster = view.findViewById(R.id.picture);
        ImageView trailer = view.findViewById(R.id.trailer);

        new DownloadImageTask(poster).execute(movie.onShow.movie.poster.href);
        title.setText(movie.onShow.movie.title);
        author.setText(movie.onShow.movie.castingShort.directors);
        actors.setText(movie.onShow.movie.castingShort.actors);
        releaseDate.setText(movie.onShow.movie.release.releaseDate);

        final String trailerUrl = movie.onShow.movie.trailer.href;
        String trailerId = trailerUrl.split("v=")[1].split("&")[0];

        new DownloadImageTask(trailer).execute("https://img.youtube.com/vi/" + trailerId + "/0.jpg");

        trailer.setClickable(true);
        trailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(trailerUrl);
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(launchBrowser);
            }
        });

        return view;
    }

    /**
     * Handle when one activity is selected
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
