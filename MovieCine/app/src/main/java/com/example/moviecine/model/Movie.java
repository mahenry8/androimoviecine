package com.example.moviecine.model;

import java.util.List;

/**
 * Movie model
 */
public class Movie {

    public class CastingShort {
        public String directors;
        public String actors;
    }

    public class Release {
        public String releaseDate;
    }

    public class Genre {
        public String name;
    }

    public class Poster {
        public String href;
    }

    public class Trailer {
        public String href;
    }

    public class Statistics {
        public float pressRating;
        public float pressReveiewCount;
        public float userRating;
        public float userReviewCount;
    }

    public String title;
    public CastingShort castingShort;
    public Release release;
    public List<Genre> genre;
    public Poster poster;
    public Trailer trailer;
    public Statistics statistics;
}
