package com.example.moviecine.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import com.example.moviecine.R;
import com.example.moviecine.fragment.ListFragment;

/**
 * Activity for list of movies
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Create the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListFragment fragment = new ListFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commitAllowingStateLoss();
    }

    /**
     * Handle action when click on one movie line
     */
    @Override
    public void onBackPressed() {
        // Get to detail fragment
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("DETAIL");
        if (fragment == null) {
            super.onBackPressed();
        } else {
            ListFragment listFragment = new ListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, listFragment).commitAllowingStateLoss();
        }
    }
}
